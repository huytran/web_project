from django.http import Http404
from django.views.generic import ListView, DetailView
from django.shortcuts import render, get_object_or_404

from products.models import Products


class ProductListView(ListView):
    queryset = Products.objects.all()
    template_name = 'products/list.html'

    # def get_context_data(self, *arg, **kwargs):
    #     context = super(ProductListView, self).get_context_data(*arg, **kwargs)
    #     print(context)
    #     return context

def product_list_view(request):
    queryset = Products.objects.all()
    context = {
        'object_list': queryset
    }
    return render(request, 'products/list.html', context)

class ProductDetailSlugView(DetailView):
    queryset = Products.objects.all()
    template_name = 'products/detail.html'
    # def get_object(self, *args, **kwargs):
    #     request = self.request
    #     slug = self.kwargs.get('slug')
    #     try:
    #         instance = Products.objects.get(slug=slug, active=True)
    #     except Products.DoesNotExist:
    #         raise Http404("Not Found")
    #     except Products.MultipleObjectsReturned:
    #         qs = Products.objects.filter(slug=slug, active=True)
    #         instance = qs.first()
    #     except:
    #         raise Http404("ummm")
    #     return instance

class ProductDetailView(DetailView):
    queryset = Products.objects.all()
    template_name = 'products/detail.html'

    # def get_context_data(self, *arg, **kwargs):
    #     context = super(ProductDetailView, self).get_context_data(*arg, **kwargs)
    #     print(context)
    #     return context

def product_detail_view(request,pk=None,  *arg, **kwargs):
    #instance= Products.objects.get(pk=pk) #id
    # instance = get_object_or_404(Products, pk=pk)
    # try:
    #     instance = Products.objects.get(id=pk)
    # except Products.DoesNotExist:
    #     print('No product here')
    #     # raise Http404("Product doesn't exist")
    # except:
    #     print("huh?")

    qs = Products.objects.filter(id=pk)
    # print(qs)
    if qs.exist() and qs.count() == 1:
        instance = qs.first()
    else:
        raise Http404("Product doesn't exist")
    context = {
        'object_list': instance
    }
    return render(request, 'products/detail.html', context)
